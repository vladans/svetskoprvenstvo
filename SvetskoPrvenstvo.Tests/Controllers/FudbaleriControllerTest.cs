﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SvetskoPrvenstvo.Interfaces;
using SvetskoPrvenstvo.Models;
using SvetskoPrvenstvo.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;
using System.Linq;

namespace SvetskoPrvenstvo.Tests.Controllers
{
    [TestClass]
    public class FudbaleriControllerTest
    {
        [TestMethod]
        public void GetReturns200AndObject()
        {
            // Arrange
            var mockRepository = new Mock<IFudbalerRepository>();
            mockRepository.Setup(x => x.GetById(50)).Returns(new Fudbaler { Id = 50, Naziv = "Test50", Godina = 1980, BrojUtakmica = 50, ReprezentacijaId = 3 });

            var controller = new FudbaleriController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(50);
            var contentResult = actionResult as OkNegotiatedContentResult<Fudbaler>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(50, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetReturns404()
        {
            // Arrange
            var mockRepository = new Mock<IFudbalerRepository>();
            var controller = new FudbaleriController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(51);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        //[TestMethod]
        //public void DeleteReturnsNotFound()
        //{
        //    // Arrange 
        //    var mockRepository = new Mock<IFudbalerRepository>();
        //    var controller = new FudbaleriController(mockRepository.Object);

        //    // Act
        //    IHttpActionResult actionResult = controller.Delete(52);

        //    // Assert
        //    Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        //}

        //[TestMethod]
        //public void DeleteReturnsOk()
        //{
        //    // Arrange
        //    var mockRepository = new Mock<IFudbalerRepository>();
        //    mockRepository.Setup(x => x.GetById(53)).Returns(new Fudbaler { Id = 53, Naziv = "Test53", Godina = 1980, BrojUtakmica = 50, ReprezentacijaId = 3 });
        //    var controller = new FudbaleriController(mockRepository.Object);

        //    // Act
        //    IHttpActionResult actionResult = controller.Delete(53);

        //    // Assert
        //    Assert.IsInstanceOfType(actionResult, typeof(OkResult));
        //}

        //[TestMethod]
        //public void PutReturnsBadRequest()
        //{
        //    //Arrange
        //   var mockRepository = new Mock<IFudbalerRepository>();
        //    var controller = new FudbaleriController(mockRepository.Object);

        //    //Act
        //   IHttpActionResult actionResult = controller.Put(54, new Fudbaler { Id = 100, Naziv = "Test100", Godina = 1980, BrojUtakmica = 50, ReprezentacijaId = 3 });

        //    //Assert
        //    Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        //}

        [TestMethod]
        //public void PostMethodSetsLocationHeader()
        public void PostReturns201AndObject()
        {
            // Arrange
            var mockRepository = new Mock<IFudbalerRepository>();
            var controller = new FudbaleriController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Fudbaler { Id = 55, Naziv = "Test55", Godina = 1980, BrojUtakmica = 50, ReprezentacijaId = 3 });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Fudbaler>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(55, createdResult.RouteValues["id"]);

            // DODATAK
            Assert.IsNotNull(createdResult.Content);
            Assert.AreEqual(55, createdResult.Content.Id);
            Assert.AreEqual("Test55", createdResult.Content.Naziv);
        }

        [TestMethod]
        public void PostReturnsMultipleObjects()
        {
            // Arrange
            List<Fudbaler> fudbaleri = new List<Fudbaler>();
            fudbaleri.Add(new Fudbaler { Id = 56, Naziv = "Test56", Godina = 1980, BrojUtakmica = 50, ReprezentacijaId = 3 });
            fudbaleri.Add(new Fudbaler { Id = 57, Naziv = "Test57", Godina = 1980, BrojUtakmica = 50, ReprezentacijaId = 3 });

            Pretraga pretraga = new Pretraga() { Start = 45, Kraj = 55};

            var mockRepository = new Mock<IFudbalerRepository>();
            mockRepository.Setup(x => x.GetPretraga(45, 55)).Returns(fudbaleri.AsEnumerable());
            var controller = new FudbaleriController(mockRepository.Object);

            // Act
            //IEnumerable<Festival> result = controller.Get();
            dynamic response = controller.PostPretraga(pretraga);
            var result = (IEnumerable<dynamic>)response.Content;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(fudbaleri.Count, result.ToList().Count);
            Assert.AreEqual(fudbaleri.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(fudbaleri.ElementAt(1), result.ElementAt(1));
            
            // DODATAK
            Assert.AreEqual(response.GetType().GetGenericTypeDefinition(), typeof(OkNegotiatedContentResult<>));
        }
    }
}
