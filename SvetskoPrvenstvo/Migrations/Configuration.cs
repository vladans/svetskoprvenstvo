namespace SvetskoPrvenstvo.Migrations
{
    using SvetskoPrvenstvo.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SvetskoPrvenstvo.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SvetskoPrvenstvo.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Reprezentacije.AddOrUpdate(x => x.Id,
                new Reprezentacija() { Id = 1, Naziv = "Brazil", Skracenica = "BRA", Godina = 1904, Titule = 5 },
                new Reprezentacija() { Id = 2, Naziv = "Nemacka", Skracenica = "GER", Godina = 1900, Titule = 3 },
                new Reprezentacija() { Id = 3, Naziv = "Srbija", Skracenica = "SRB", Godina = 1905, Titule = 0 }
            );

            context.Fudbaleri.AddOrUpdate(x => x.Id,
                new Fudbaler() { Id = 1, Naziv = "Branislav Ivanovic", Godina = 1984, BrojUtakmica = 105, ReprezentacijaId = 3 },
                new Fudbaler() { Id = 2, Naziv = "Neymar", Godina = 1992, BrojUtakmica = 90, ReprezentacijaId = 1 },
                new Fudbaler() { Id = 3, Naziv = "Coutinho", Godina = 1992, BrojUtakmica = 41, ReprezentacijaId = 1 },
                new Fudbaler() { Id = 4, Naziv = "Manuel Neuer", Godina = 1986, BrojUtakmica = 79, ReprezentacijaId = 2 }
            );
        }
    }
}
