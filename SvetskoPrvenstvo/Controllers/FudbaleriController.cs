﻿using SvetskoPrvenstvo.Interfaces;
using SvetskoPrvenstvo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SvetskoPrvenstvo.Controllers
{
    public class FudbaleriController : ApiController
    {
        IFudbalerRepository _repository { get; set; }
        public FudbaleriController(IFudbalerRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Fudbaler> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var fudbaler = _repository.GetById(id);
            if (fudbaler == null)
            {
                return NotFound();
            }
            return Ok(fudbaler);
        }

        //public IEnumerable<Fudbaler> GetGodina(int godina)
        public IHttpActionResult GetGodina(int godina)
        {
            var fudbaleri = _repository.GetByGodina(godina);
            if (fudbaleri == null || !fudbaleri.Any())
            {
                return NotFound();
            }
            return Ok(fudbaleri);
        }

        public IHttpActionResult Post(Fudbaler fudbaler)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(fudbaler);
            return CreatedAtRoute("DefaultApi", new { id = fudbaler.Id }, fudbaler);
        }

        [Authorize]
        [Route("api/pretraga")]
        public IHttpActionResult PostPretraga(Pretraga pretraga)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if(pretraga.Start > pretraga.Kraj)
            {
                return BadRequest("Start ne sme biti veci od kraja pretrage");
            }
            var festivali = _repository.GetPretraga(pretraga.Start, pretraga.Kraj);
            if (festivali == null || !festivali.Any())
            {
                return NotFound();
            }
            return Ok(festivali);
        }

        [Authorize]
        public IHttpActionResult Put(int id, Fudbaler fudbaler)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fudbaler.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(fudbaler);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(fudbaler);
        }

        [Authorize]
        [Route("api/marketi/{id}")]
        public IHttpActionResult Delete(int id)
        {
            var fudbaler = _repository.GetById(id);
            if (fudbaler == null)
            {
                return NotFound();
            }

            _repository.Delete(fudbaler);
            return Ok();
        }
    }
}
