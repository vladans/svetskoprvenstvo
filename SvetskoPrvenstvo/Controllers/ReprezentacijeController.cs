﻿using SvetskoPrvenstvo.Interfaces;
using SvetskoPrvenstvo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SvetskoPrvenstvo.Controllers
{
    public class ReprezentacijeController : ApiController
    {
        IReprezentacijaRepository _repository { get; set; }
        public ReprezentacijeController(IReprezentacijaRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Reprezentacija> Get()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult Get(int id)
        {
            var reprezentacija = _repository.GetById(id);
            if (reprezentacija == null)
            {
                return NotFound();
            }
            return Ok(reprezentacija);
        }

        //[Authorize]
        [Route("api/najbolji")]
        public IEnumerable<Reprezentacija> GetNajbolji()
        {
            return _repository.GetNajbolji();
        }

        //public IEnumerable<Reprezentacija> GetSaManjeTitula(int titula)
        //{
        //    //var reprezentacije = _repository.GetByTitula(titula);
        //    //if (reprezentacije == null)
        //    //{
        //    //    return NotFound();
        //    //}
        //    //return reprezentacije;
        //    return _repository.GetByTitula(titula);
        //}

        //public IHttpActionResult Post(Reprezentacija reprezentacija)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    _repository.Add(reprezentacija);
        //    return CreatedAtRoute("DefaultApi", new { id = reprezentacija.Id }, reprezentacija);
        //}

        //public IHttpActionResult Put(int id, Reprezentacija reprezentacija)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != reprezentacija.Id)
        //    {
        //        return BadRequest();
        //    }

        //    try
        //    {
        //        _repository.Update(reprezentacija);
        //    }
        //    catch
        //    {
        //        return BadRequest();
        //    }

        //    return Ok(reprezentacija);
        //}

        //public IHttpActionResult Delete(int id)
        //{
        //    var reprezentacija = _repository.GetById(id);
        //    if (reprezentacija == null)
        //    {
        //        return NotFound();
        //    }

        //    _repository.Delete(reprezentacija);
        //    return Ok();
        //}
    }
}
