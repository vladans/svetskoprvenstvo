﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SvetskoPrvenstvo.Models
{
    public class Reprezentacija
    {
        public int Id { get; set; }

        public string Naziv { get; set; }

        [StringLength(3, MinimumLength = 3, ErrorMessage = "Unesite tekstualnu vrednost sa tacno 3 karaktera")]
        public string Skracenica { get; set; }

        [Range(1851, 1999, ErrorMessage = "Unesite numericku vrednost manju od 2000, a vecu od 1850")]
        public int Godina { get; set; }

        [Range(0, 5, ErrorMessage = "Unesite numericku vrednost manju od 6, a vecu ili jednaku 0")]
        public int Titule { get; set; }
    }
}