﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SvetskoPrvenstvo.Models
{
    public class Fudbaler
    {
        public int Id { get; set; }

        [StringLength(40, ErrorMessage = "Unesite tekstualnu vrednost sa najvise 40 karaktera.")]
        public string Naziv { get; set; }

        [Range(1976, 1999, ErrorMessage = "Unesite numericku vrednost manju od 2000, a vecu od 1975.")]
        public int Godina { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Unesite numericku vrednost vecu od 0.")]
        public int BrojUtakmica { get; set; }

        public int ReprezentacijaId { get; set; }
        public Reprezentacija Reprezentacija { get; set; }

    }
}