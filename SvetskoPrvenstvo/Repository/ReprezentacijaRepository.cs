﻿using SvetskoPrvenstvo.Interfaces;
using SvetskoPrvenstvo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace SvetskoPrvenstvo.Repository
{
    public class ReprezentacijaRepository : IDisposable, IReprezentacijaRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        //public bool Add(Reprezentacija reprezentacija)
        //{
        //    db.Reprezentacije.Add(reprezentacija);
        //    int res = db.SaveChanges();
        //    return res > 0 ? true : false;
        //}

        //public bool Delete(Reprezentacija reprezentacija)
        //{
        //    db.Reprezentacije.Remove(reprezentacija);
        //    int res = db.SaveChanges();
        //    return res > 0 ? true : false;
        //}

        public IEnumerable<Reprezentacija> GetAll()
        {
            return db.Reprezentacije;
        }

        public Reprezentacija GetById(int id)
        {
            return db.Reprezentacije.FirstOrDefault(x => x.Id == id);
        }

        //prikaz dve reprezentacije sa najviše osvojenih titula, sortirano po broju titula opadajuće
        public IEnumerable<Reprezentacija> GetNajbolji()
        {
            return db.Reprezentacije.OrderByDescending(x => x.Titule).Take(2);
        }

        //public IEnumerable<Reprezentacija> GetByTitula(int titula)
        //{
        //    return db.Reprezentacije.Where(x => x.Titule < titula).OrderBy(x => x.Titule);
        //}

        //public bool Update(Reprezentacija reprezentacija)
        //{
        //    db.Entry(reprezentacija).State = EntityState.Modified;

        //    try
        //    {
        //        int res = db.SaveChanges();
        //        return res > 0 ? true : false;
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        throw;
        //    }
        //}

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}