﻿using SvetskoPrvenstvo.Interfaces;
using SvetskoPrvenstvo.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace SvetskoPrvenstvo.Repository
{
    public class FudbalerRepository : IDisposable, IFudbalerRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public bool Add(Fudbaler fudbaler)
        {
            db.Fudbaleri.Add(fudbaler);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public bool Delete(Fudbaler fudbaler)
        {
            db.Fudbaleri.Remove(fudbaler);
            int res = db.SaveChanges();
            return res > 0 ? true : false;
        }

        public IEnumerable<Fudbaler> GetAll()
        {
            return db.Fudbaleri.Include(x => x.Reprezentacija).OrderByDescending(x => x.BrojUtakmica);
        }

        public Fudbaler GetById(int id)
        {
            return db.Fudbaleri.FirstOrDefault(x => x.Id == id);
        }

        // preuzimanje svih fudbalera koji su rođeni nakon prosleđene godine, sortirano po godini rođenja rastuće
        public IEnumerable<Fudbaler> GetByGodina(int godina)
        {
            return db.Fudbaleri
                .Include(x => x.Reprezentacija)
                .Where(x => x.Godina > godina)
                .OrderBy(x => x.Godina);
        }

        // preuzimanje fudbalera sa brojem utakmica za reprezentaciju između dve unete vrednosti
        // (​start ​i ​kraj​), sortiranih po broju utakmica rastuće
        public IEnumerable<Fudbaler> GetPretraga(int start, int kraj)
        {
            return db.Fudbaleri
                .Include(x => x.Reprezentacija)
                .Where(x => x.BrojUtakmica > start && x.BrojUtakmica < kraj)
                .OrderBy(x => x.BrojUtakmica);
        }

        public bool Update(Fudbaler fudbaler)
        {
            db.Entry(fudbaler).State = EntityState.Modified;
            try
            {
                int res = db.SaveChanges();
                return res > 0 ? true : false;
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}