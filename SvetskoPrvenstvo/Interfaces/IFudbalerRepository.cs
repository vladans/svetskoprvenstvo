﻿using SvetskoPrvenstvo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvetskoPrvenstvo.Interfaces
{
    public interface IFudbalerRepository
    {
        IEnumerable<Fudbaler> GetAll();
        Fudbaler GetById(int id);
        IEnumerable<Fudbaler> GetByGodina(int godina);
        IEnumerable<Fudbaler> GetPretraga(int start, int kraj);
        bool Add(Fudbaler fudbaler);
        bool Update(Fudbaler fudbaler);
        bool Delete(Fudbaler fudbaler);
    }
}
