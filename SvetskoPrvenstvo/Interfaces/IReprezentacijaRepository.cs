﻿using SvetskoPrvenstvo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SvetskoPrvenstvo.Interfaces
{
    public interface IReprezentacijaRepository
    {
        IEnumerable<Reprezentacija> GetAll();
        Reprezentacija GetById(int id);
        IEnumerable<Reprezentacija> GetNajbolji();
        //IEnumerable<Reprezentacija> GetByTitula(int titula);
        //bool Add(Reprezentacija reprezentacija);
        //bool Update(Reprezentacija reprezentacija);
        //bool Delete(Reprezentacija reprezentacija);
    }
}
